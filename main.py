import numpy as np
import cv2 as cv
import time

video_path = "/Users/nguyenphu/Desktop/Desktop/gitlab/Traffic-flow/video/tiensa.mp4"
cap = cv.VideoCapture(video_path)
# params for ShiTomasi corner detection
feature_params = dict( maxCorners = 100,
                       qualityLevel = 0.3,
                       minDistance = 7,
                       blockSize = 7 )

# Parameters for lucas kanade optical flow
lk_params = dict( winSize  = (15,15),
                  maxLevel = 2,
                  criteria = (cv.TERM_CRITERIA_EPS | cv.TERM_CRITERIA_COUNT, 10, 0.03))
# Create some random colors
color = np.random.randint(0,255,(100,3))
# Take first frame and find corners in it
ret, old_frame = cap.read()
old_gray = cv.cvtColor(old_frame, cv.COLOR_BGR2GRAY)
p0 = cv.goodFeaturesToTrack(old_gray, mask = None, **feature_params)
#p0 = np.array([[[300, 300]]], dtype = np.float32)
# p0 = np.append(p0, [[[float(123), float(4)]]], axis=0)
# p0 = np.append(p0, [[[float(4), float(2)]]], axis=0)
# # #Create a mask image for drawing purposes
#p0 = np.array([[[w,w]] for w in range(400, 500, 20)], dtype = np.float32)
mask = np.zeros_like(old_frame)
number_frame = 0
object = 0
while(1):
    number_frame += 1
    ret,frame = cap.read()
    frame_gray = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
    p1, st, err = cv.calcOpticalFlowPyrLK(old_gray, frame_gray, p0, None, **lk_params)

    good_new = p1
    good_old = p0

    for i,(new,old) in enumerate(zip(good_new,good_old)):
        a,b = new.ravel()
        c,d = old.ravel()
        #mask = cv.line(mask, (a,b), (c,d), color[i].tolist(), 2)
        #frame = cv.circle(frame, (a,b), 5, color[i].tolist(), -1)
        cv.putText(frame,str(i),(a,b),cv.FONT_HERSHEY_SIMPLEX,1,color = (200,50,75),thickness = 3)
    img = cv.add(frame, mask)
    cv.imshow("mask", mask)
    cv.imshow('frame',img)
    k = cv.waitKey(0) & 0xff
    if k == 27:
        break
    # Now update the previous frame and previous points
    old_gray = frame_gray.copy()
    p0 = good_new.reshape(-1,1,2)
    if number_frame%20==0:
        p0 = cv.goodFeaturesToTrack(old_gray, mask = None, **feature_params)
    
cv.waitKey(0)
cv.destroyAllWindows()
cap.release()